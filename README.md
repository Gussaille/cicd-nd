## Description

Devops continuous integration and continuous delivery

## Teams members

- Nicolas Durocher

## What has been done

- Pipelines triggered at change: working test and deployment jobs

## Deployment URLs

- https://devops-cicd-nd.herokuapp.com/
